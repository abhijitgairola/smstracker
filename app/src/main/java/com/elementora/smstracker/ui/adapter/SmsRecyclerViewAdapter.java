package com.elementora.smstracker.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elementora.smstracker.R;
import com.elementora.smstracker.model.SmsModel;
import com.elementora.smstracker.ui.fragment.ClassifiedFragment.OnListFragmentInteractionListener;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link com.elementora.smstracker.model.SmsModel} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class SmsRecyclerViewAdapter extends RecyclerView.Adapter<SmsRecyclerViewAdapter.ViewHolder> {

	private final List<SmsModel> mValues;
	private final OnListFragmentInteractionListener mListener;

	public SmsRecyclerViewAdapter(List<SmsModel> items, OnListFragmentInteractionListener listener) {
		mValues = items;
		mListener = listener;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.fragment_classifiedsms, parent, false);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, int position) {
		holder.mItem = mValues.get(position);
		String title = ((holder.mItem.getSenderModel()!=null)?holder.mItem.getSenderModel().getName():"") + ((holder.mItem.getSmsCategoryModel()!=null)? " --> "+holder.mItem.getSmsCategoryModel().getCategoryName():"");
		holder.mIdView.setText(title);
		holder.mContentView.setText(StringUtils.abbreviate(mValues.get(position).getRawData(), 80));

		holder.mView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (null != mListener) {
					// Notify the active callbacks interface (the activity, if the
					// fragment is attached to one) that an item has been selected.
					mListener.onListFragmentInteraction(holder.mItem);
				}
			}
		});
	}

	@Override
	public int getItemCount() {
		return mValues.size();
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		public final View mView;
		public final TextView mIdView;
		public final TextView mContentView;
		public SmsModel mItem;

		public ViewHolder(View view) {
			super(view);
			mView = view;
			mIdView = (TextView) view.findViewById(R.id.id);
			mContentView = (TextView) view.findViewById(R.id.content);
		}

		@Override
		public String toString() {
			return super.toString() + " '" + mContentView.getText() + "'";
		}
	}
}
