package com.elementora.smstracker.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.elementora.smstracker.model.SmsModel;
import com.elementora.smstracker.ui.fragment.ClassifiedFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abhijit on 02/03/16.
 */
public class MainPagerAdapter extends FragmentStatePagerAdapter {

	List<ClassifiedFragment> registeredFragments = new ArrayList<>();

	public MainPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		ClassifiedFragment classifiedFragment = new ClassifiedFragment();
		switch (position) {
			case 0:
				classifiedFragment.setListItems(SmsModel.getUnclassified(20));
				break;
			case 1:
				classifiedFragment.setListItems(SmsModel.getClassifiedByHuman());
				break;
			case 2:
				classifiedFragment.setListItems(SmsModel.getClassifiedBySystem());
				break;
		}
		return classifiedFragment;
	}

	@Override
	public int getCount() {
		return 3;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		switch (position){
			case 0:
				return "Unclassified";
			case 1:
				return "Human";
			case 2:
				return "System";
		}
		return "";
	}

//	@Override
//	public int getItemPosition(Object object) {
//		return POSITION_NONE;
//	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		ClassifiedFragment fragment =  (ClassifiedFragment)super.instantiateItem(container, position);
		registeredFragments.add(fragment);
		return fragment;
	}

	public void refreshInnerListViews() {
		if (registeredFragments == null)
			return;
		int position = 0;
		for (ClassifiedFragment classifiedFragment : registeredFragments) {
			switch (position) {
				case 0:
					classifiedFragment.setListItems(SmsModel.getUnclassified(20));
					break;
				case 1:
					classifiedFragment.setListItems(SmsModel.getClassifiedByHuman());
					break;
				case 2:
					classifiedFragment.setListItems(SmsModel.getClassifiedBySystem());
					break;
			}
			classifiedFragment.refreshListView();
			position++;
		}
		this.notifyDataSetChanged();
	}
}
