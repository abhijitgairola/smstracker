package com.elementora.smstracker.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.elementora.smstracker.R;
import com.elementora.smstracker.core.classification.cursor.WholeDatabase;
import com.elementora.smstracker.core.classification.job.InboxClassifierJob;
import com.elementora.smstracker.core.classification.job.InitialFillJob;
import com.elementora.smstracker.model.SmsModel;
import com.elementora.smstracker.ui.adapter.MainPagerAdapter;
import com.elementora.smstracker.ui.fragment.ClassifiedFragment;
import com.elementora.smstracker.ui.view.SlidingTabLayout;

public class MainActivity extends AppCompatActivity implements InitialFillJob.JobCallback, ClassifiedFragment.OnListFragmentInteractionListener, InboxClassifierJob.JobCallback {
	private static final String TAG = MainActivity.class.getSimpleName();

	private ViewPager mViewPager;
	private SlidingTabLayout tabLayout;

	private MainPagerAdapter mainPagerAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (InitialFillJob.isInitialRunDone()) {
			this.initialDataFillJobDone();
		} else {
			new InitialFillJob(this, this).execute();
		}
	}

	@Override
	public void initialDataFillJobDone() {
		//Runs on UI thread
		if (InboxClassifierJob.isInboxScanDone()) {
			this.classificationJobDone();
		} else {
			new InboxClassifierJob(this, this).execute(new WholeDatabase(this));
		}
	}

	@Override
	public void classificationJobDone() {
		mainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());

		mViewPager = (ViewPager) findViewById(R.id.container);
		mViewPager.setAdapter(mainPagerAdapter);

		tabLayout = (SlidingTabLayout) findViewById(R.id.tabs);
		tabLayout.setDistributeEvenly(false);
		tabLayout.setViewPager(mViewPager);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mViewPager != null) {
			mainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
			mViewPager.setAdapter(mainPagerAdapter);
		}
//		if (mainPagerAdapter != null)
//			mainPagerAdapter.refreshInnerListViews();
	}

	@Override
	public void onListFragmentInteraction(SmsModel item) {
		Log.i(TAG, "onListFragmentInteraction = " + item);
		Intent intent = new Intent(this, UserInputActivity.class);
		intent.putExtra("smsId", item.getSmsId());
		startActivityForResult(intent, 1);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 1 && requestCode == RESULT_OK) {
			mainPagerAdapter.notifyDataSetChanged();
		}
	}
}
