package com.elementora.smstracker.ui.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.elementora.smstracker.R;
import com.elementora.smstracker.model.GroupModel;
import com.elementora.smstracker.model.SenderModel;
import com.elementora.smstracker.model.SmsCategoryModel;
import com.elementora.smstracker.model.SmsModel;

import org.apache.commons.lang3.StringUtils;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserInputFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class UserInputFragment extends Fragment {

	private OnFragmentInteractionListener mListener;

	private ViewHolder viewHolder;

	private SmsModel smsModel;

	public UserInputFragment() {
		// Required empty public constructor
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View rootView = inflater.inflate(R.layout.fragment_user_input, container, false);
		setHasOptionsMenu(true);

		viewHolder = new ViewHolder(rootView, getContext());
		long smsId = getArguments().getLong("smsId");
		smsModel = SmsModel.getBySmsId(smsId);

		try{
			viewHolder.etMainGroup.setText(smsModel.getSenderModel().getGroupModel().getGroupName());
		} catch (NullPointerException e) {
			viewHolder.etMainGroup.setText("");
		}
		try {
			viewHolder.etSender.setText(smsModel.getSenderModel().getName());
		} catch (NullPointerException e) {
			viewHolder.etSender.setText("");
		}

		if (smsModel.getSmsCategoryModel() != null)
			viewHolder.etCategory.setText(smsModel.getSmsCategoryModel().getCategoryName());

		viewHolder.tvTimestamp.setText(smsModel.getReceivedTime().toString());
		viewHolder.tvSmsRawData.setText(smsModel.getRawData());

		//This should be done after viewholder is initialized
		viewHolder.refreshAutoCompleteAdaptors();

		return rootView;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
		inflater.inflate(R.menu.saveclassification, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
			case R.id.menu_action_save_classification:
				saveClassification();
		}
		return super.onOptionsItemSelected(item);
	}

	private void saveClassification() {
		//Saving this classification by user
		String group = viewHolder.etMainGroup.getText().toString();
		String sender = viewHolder.etSender.getText().toString();
		String category = viewHolder.etCategory.getText().toString();

		if (StringUtils.isBlank(group) || StringUtils.isBlank(sender) || StringUtils.isBlank(category)) {
			Toast.makeText(getContext(), "Please fill details", Toast.LENGTH_SHORT).show();
			return;
		}

		GroupModel groupModel = GroupModel.getByGroupName(group);
		if (groupModel == null) {
			groupModel = new GroupModel();
			groupModel.setGroupName(group);
			groupModel.setDisplayName(group);
			groupModel.save();
		}

		SenderModel senderModel = SenderModel.getByNameAndGroup(sender, group);
		if (senderModel == null) {
			senderModel = new SenderModel();
			senderModel.setName(sender);
			senderModel.setIdentifier(smsModel.getSenderName());
			senderModel.setGroupModel(groupModel);
			senderModel.save();
		}

		SmsCategoryModel smsCategoryModel = SmsCategoryModel.getByCategoryAndGroup(category, group);
		if (smsCategoryModel == null){
			smsCategoryModel = new SmsCategoryModel();
			smsCategoryModel.setCategoryName(category);
			smsCategoryModel.setGroupModel(groupModel);
			smsCategoryModel.save();
		}

		smsModel.setSenderModel(senderModel);
		smsModel.setSenderName(senderModel.getIdentifier());
		smsModel.setSmsCategoryModel(smsCategoryModel);
		smsModel.setCategorisedBy(SmsModel.CLASSIFIED_BY.HUMAN);
		smsModel.setClassificationVerified(true);
		smsModel.save();

		Toast.makeText(getActivity(), "Successfully saved", Toast.LENGTH_SHORT).show();
		if (mListener != null)
			mListener.classificationSavedSuccessfully(smsModel);
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof OnFragmentInteractionListener) {
			mListener = (OnFragmentInteractionListener) context;
		} else {
			throw new RuntimeException(context.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated
	 * to the activity and potentially other fragments contained in that
	 * activity.
	 * <p/>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentInteractionListener {
		void classificationSavedSuccessfully(SmsModel smsModel);
	}

	private class ViewHolder{
		AutoCompleteTextView etMainGroup, etSender, etCategory;
		TextView tvTimestamp, tvSmsRawData;
		View categoryContainer;
		Context context;

		public ViewHolder(View rootView, Context context) {
			etMainGroup = (AutoCompleteTextView) rootView.findViewById(R.id.et_main_group);
			etSender = (AutoCompleteTextView) rootView.findViewById(R.id.et_sender);
			etCategory = (AutoCompleteTextView) rootView.findViewById(R.id.et_category);
			tvTimestamp = (TextView) rootView.findViewById(R.id.tv_timestamp);
			tvSmsRawData = (TextView) rootView.findViewById(R.id.et_sms_raw_data);
			categoryContainer = rootView.findViewById(R.id.category_container);
			this.context = context;

			ArrayAdapter<String> groupAdapter = new ArrayAdapter<>(context,
					android.R.layout.simple_dropdown_item_1line, GroupModel.getGroupnamesList());
			etMainGroup.setAdapter(groupAdapter);

			etMainGroup.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					onGroupChange(s.toString());
				}
				@Override
				public void afterTextChanged(Editable s) {}
			});

			etSender.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					onSenderChange(s.toString());
				}
				@Override
				public void afterTextChanged(Editable s) {}
			});
		}

		public void refreshAutoCompleteAdaptors() {
			onGroupChange(etMainGroup.getText().toString(), false);
			onSenderChange(etSender.getText().toString(), false);
		}

		private void onGroupChange(String newValue) {
			onGroupChange(newValue, true);
		}
		private void onGroupChange(String newValue, boolean clear) {
			//Refreshing sender adaptor
			ArrayAdapter<String> senderAdapter = new ArrayAdapter<>(context,
					android.R.layout.simple_dropdown_item_1line, SenderModel.getAllDistinctSender(newValue));
			etSender.setAdapter(senderAdapter);
			if (clear) {
				etSender.setText("");
				etCategory.setText("");
			}
		}

		private void onSenderChange(String newValue) {
			onSenderChange(newValue, true);
		}
		private void onSenderChange(String newValue, boolean clear) {
			//Refreshing category adaptor
			String groupName = etMainGroup.getText().toString();
			ArrayAdapter<String> categoryAdapter = new ArrayAdapter<>(context,
					android.R.layout.simple_dropdown_item_1line, SmsCategoryModel.getAllCategory(newValue, groupName));
			etCategory.setAdapter(categoryAdapter);
			if (clear) {
				etCategory.setText("");
			}
		}
	}
}
