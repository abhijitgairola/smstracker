package com.elementora.smstracker.ui.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.afollestad.materialdialogs.MaterialDialog;
import com.elementora.smstracker.R;
import com.elementora.smstracker.core.classification.event.HumanClassifiedEvent;
import com.elementora.smstracker.core.classification.event.HumanClassifiedEventCallback;
import com.elementora.smstracker.model.SmsModel;
import com.elementora.smstracker.ui.fragment.UserInputFragment;

public class UserInputActivity extends AppCompatActivity implements UserInputFragment.OnFragmentInteractionListener, HumanClassifiedEventCallback{

	ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_input);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		UserInputFragment userInputFragment = new UserInputFragment();
		userInputFragment.setArguments(getIntent().getExtras());
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.container, userInputFragment)
				.commit();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.homeAsUp:
			case R.id.home:
			case android.R.id.home:
				onBackPressed();
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void classificationSavedSuccessfully(SmsModel smsModel) {
		progressDialog = ProgressDialog.show(this, "Please wait!", "Creating rules...");

		Intent intent = new Intent("com.elementora.smsclassified");
		intent.putExtra("smsId", smsModel.getSmsId());
		HumanClassifiedEvent classifiedEvent = new HumanClassifiedEvent(this);
		classifiedEvent.onReceive(this, intent);
	}

	@Override
	public void actionCompleted(String message) {
		if (progressDialog != null)
			progressDialog.dismiss();
		new MaterialDialog.Builder(this)
				.content(message)
				.positiveText("OK")
				.dismissListener(new DialogInterface.OnDismissListener() {
					@Override
					public void onDismiss(DialogInterface dialog) {
						onBackPressed();
					}
				})
				.callback(new MaterialDialog.ButtonCallback() {
					@Override
					public void onPositive(MaterialDialog dialog) {
						onBackPressed();
					}
				})
				.cancelable(false)
				.show();

	}
}
