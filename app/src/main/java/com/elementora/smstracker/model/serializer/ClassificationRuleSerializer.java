package com.elementora.smstracker.model.serializer;

import com.activeandroid.serializer.TypeSerializer;
import com.elementora.smstracker.model.data.ClassificationRule;
import com.google.gson.Gson;

/**
 * Created by abhijit on 29/02/16.
 */
public class ClassificationRuleSerializer extends TypeSerializer {

	@Override
	public Class<?> getDeserializedType() {
		return ClassificationRule.class;
	}

	@Override
	public Class<?> getSerializedType() {
		return String.class;
	}

	@Override
	public String serialize(Object data) {
		if (data == null)
			return null;
		Gson gson = new Gson();
		return gson.toJson(data, ClassificationRule.class);
	}

	@Override
	public ClassificationRule deserialize(Object data) {
		if (data == null)
			return null;
		Gson gson = new Gson();
		return gson.fromJson((String) data, ClassificationRule.class);
	}
}
