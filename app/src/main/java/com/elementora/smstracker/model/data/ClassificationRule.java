package com.elementora.smstracker.model.data;

import java.util.List;

/**
 * Created by abhijit on 29/02/16.
 */
public class ClassificationRule {
	List<String> wantedKeywords, unwantedKeywords;
	String wantedRegex;
	String unwantedRegex;

	public ClassificationRule(List<String> wantedKeywords, List<String> unwantedKeywords, String wantedRegex, String unwantedRegex) {
		this.wantedKeywords = wantedKeywords;
		this.unwantedKeywords = unwantedKeywords;
		this.wantedRegex = wantedRegex;
		this.unwantedRegex = unwantedRegex;
	}

	public List<String> getWantedKeywords() {
		return wantedKeywords;
	}

	public List<String> getUnwantedKeywords() {
		return unwantedKeywords;
	}

	public String getWantedRegex() {
		return wantedRegex;
	}

	public String getUnwantedRegex() {
		return unwantedRegex;
	}

	public void setWantedKeywords(List<String> wantedKeywords) {
		this.wantedKeywords = wantedKeywords;
	}

	public void setUnwantedKeywords(List<String> unwantedKeywords) {
		this.unwantedKeywords = unwantedKeywords;
	}

	public void setWantedRegex(String wantedRegex) {
		this.wantedRegex = wantedRegex;
	}

	public void setUnwantedRegex(String unwantedRegex) {
		this.unwantedRegex = unwantedRegex;
	}
}
