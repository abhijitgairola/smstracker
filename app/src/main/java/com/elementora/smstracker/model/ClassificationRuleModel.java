package com.elementora.smstracker.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.elementora.smstracker.model.data.ClassificationRule;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abhijit on 29/02/16.
 */
@Table(name = "ClassificationRuleModel")
public class ClassificationRuleModel extends Model{

	@Column(name = "smsCategoryModel", onDelete = Column.ForeignKeyAction.SET_NULL, onUpdate = Column.ForeignKeyAction.CASCADE, index = true)
	SmsCategoryModel smsCategoryModel;

	@Column(name = "rule")
	ClassificationRule classificationRule;

	public ClassificationRuleModel() {
		super();
	}

	public SmsCategoryModel getSmsCategoryModel() {
		return smsCategoryModel;
	}

	public void setSmsCategoryModel(SmsCategoryModel smsCategoryModel) {
		this.smsCategoryModel = smsCategoryModel;
	}

	public ClassificationRule getClassificationRule() {
		return classificationRule;
	}

	public void setClassificationRule(ClassificationRule classificationRule) {
		this.classificationRule = classificationRule;
	}



	public static List<ClassificationRuleModel> getAllRulesByGroup(SmsCategoryModel model) {
		try {
			List<SmsCategoryModel> smsCategoryModelList = new Select().from(SmsCategoryModel.class).where("groupModel = ?", model.getGroupModel().getId()).execute();

			List<ClassificationRuleModel> toReturnList = new ArrayList<>();

			for (SmsCategoryModel smsCategoryModel: smsCategoryModelList) {
				List<ClassificationRuleModel> modelList = new Select().from(ClassificationRuleModel.class).where("smsCategoryModel = ?", smsCategoryModel.getId()).execute();
				if (modelList != null) {
					toReturnList.addAll(modelList);
				}
			}
			return toReturnList;

		} catch (Exception e) {
			return new ArrayList<>();
		}
	}
}
