package com.elementora.smstracker.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abhijit on 29/02/16.
 */
@Table(name = "GroupModel")
public class GroupModel extends Model{

	@Column(name = "groupName", unique = true, onUniqueConflict = Column.ConflictAction.FAIL)
	String groupName;

	@Column(name = "displayName")
	String displayName;

	@Column(name = "description")
	String description;

	public GroupModel() {
		super();
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	/*
	DB Operation functions
	 */
	public static GroupModel getByGroupName(String groupName) {
		return new Select().from(GroupModel.class).where("groupName = ?", groupName).executeSingle();
	}

	public static String[] getGroupnamesList() {
		List<GroupModel> groupModelList =  new Select().from(GroupModel.class).execute();
		String[] toReturn = new String[groupModelList.size()];
		int i = 0;
		for (GroupModel groupModel : groupModelList) {
			toReturn[i++] = groupModel.getGroupName();
		}
		return toReturn;
	}
}
