package com.elementora.smstracker.model.serializer;

import com.activeandroid.serializer.TypeSerializer;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by abhijit on 08/03/16.
 */
public class MapSerializer extends TypeSerializer {
	@Override
	public Class<?> getDeserializedType() {
		return Map.class;
	}

	@Override
	public Class<?> getSerializedType() {
		return String.class;
	}

	@Override
	public String serialize(Object o) {
		if (o == null) {
			return null;
		}
		Gson gson = new Gson();
		return gson.toJson(o, Map.class);
	}

	@Override
	public Map<String, Object> deserialize(Object o) {
		if (o == null) {
			return null;
		}
		return new Gson().fromJson((String) o, HashMap.class);
	}
}
