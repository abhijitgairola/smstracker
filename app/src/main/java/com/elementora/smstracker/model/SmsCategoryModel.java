package com.elementora.smstracker.model;

import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import org.apache.commons.lang3.exception.ExceptionUtils;

import java.util.List;

/**
 * Created by abhijit on 29/02/16.
 */
@Table(name = "SmsCategoryModel")
public class SmsCategoryModel extends Model {
	private static final String TAG = SmsCategoryModel.class.getSimpleName();

	@Column(name = "groupModel", onDelete = Column.ForeignKeyAction.SET_NULL, onUpdate = Column.ForeignKeyAction.CASCADE, index = true, uniqueGroups = {"senderCateIdx"}, onUniqueConflict = Column.ConflictAction.FAIL)
	GroupModel groupModel;

	@Column(name = "categoryName", uniqueGroups = {"senderCateIdx"}, onUniqueConflict = Column.ConflictAction.FAIL)
	String categoryName;

	public SmsCategoryModel() {
		super();
	}

	public GroupModel getGroupModel() {
		return groupModel;
	}

	public void setGroupModel(GroupModel groupModel) {
		this.groupModel = groupModel;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	/*
	DB Operation functions
	 */
	public static String[] getAllCategory(String senderName, String group) {
		SenderModel senderModel = SenderModel.getByNameAndGroup(senderName, group);
		if (senderModel == null)
			return new String[]{};
		try {
			List<SmsCategoryModel> smsCategoryModelList = new Select().from(SmsCategoryModel.class).where("groupModel = ?", senderModel.getGroupModel().getId()).execute();
			String[] strings = new String[smsCategoryModelList.size()];
			int i = 0;
			for (SmsCategoryModel smsCategoryModel : smsCategoryModelList) {
				strings[i++] = smsCategoryModel.getCategoryName();
			}
			return strings;
		} catch (Exception e){
			Log.e(TAG, ExceptionUtils.getStackTrace(e));
		}
		return new String[]{};
	}
//
	public static SmsCategoryModel getByCategoryAndGroup(String categoryName, String groupName) {
		GroupModel groupModel = GroupModel.getByGroupName(groupName);
		if (groupModel == null)
			return null;
		return new Select().from(SmsCategoryModel.class).where("categoryName = ? and groupModel = ?", categoryName, groupModel.getId()).executeSingle();
	}
}
