package com.elementora.smstracker.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.activeandroid.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by abhijit on 29/02/16.
 */
@Table(name = "SmsModel")
public class SmsModel extends Model{
	private static final String TAG = SmsModel.class.getSimpleName();

	public enum CLASSIFIED_BY {
		HUMAN, SYSTEM
	}

	@Column(name = "smsId", unique = true, index = true, onUniqueConflict = Column.ConflictAction.FAIL)
	long smsId;

	@Column(name = "rawData")
	String rawData;

	@Column(name = "smsCategoryModel", onDelete = Column.ForeignKeyAction.SET_NULL, onUpdate = Column.ForeignKeyAction.CASCADE, index = true)
	SmsCategoryModel smsCategoryModel;

	@Column(name = "senderModel", onDelete = Column.ForeignKeyAction.SET_NULL, onUpdate = Column.ForeignKeyAction.CASCADE, index = true)
	SenderModel senderModel;

	@Column(name = "senderName")
	String senderName;

	@Column(name = "receivedTime")
	Date receivedTime;

	@Column(name = "categorisedBy")
	CLASSIFIED_BY categorisedBy;

	@Column(name = "classificationVerified")
	boolean classificationVerified;

	public SmsModel() {
		super();
	}

	public long getSmsId() {
		return smsId;
	}

	public void setSmsId(long smsId) {
		this.smsId = smsId;
	}

	public String getRawData() {
		return rawData;
	}

	public void setRawData(String rawData) {
		this.rawData = rawData;
	}

	public SmsCategoryModel getSmsCategoryModel() {
		return smsCategoryModel;
	}

	public void setSmsCategoryModel(SmsCategoryModel smsCategoryModel) {
		this.smsCategoryModel = smsCategoryModel;
	}

	public Date getReceivedTime() {
		return receivedTime;
	}

	public void setReceivedTime(Date receivedTime) {
		this.receivedTime = receivedTime;
	}

	public SenderModel getSenderModel() {
		return senderModel;
	}

	public void setSenderModel(SenderModel senderModel) {
		this.senderModel = senderModel;
	}

	public CLASSIFIED_BY getCategorisedBy() {
		return categorisedBy;
	}

	public void setCategorisedBy(CLASSIFIED_BY categorisedBy) {
		this.categorisedBy = categorisedBy;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public boolean isClassificationVerified() {
		return classificationVerified;
	}

	public void setClassificationVerified(boolean classificationVerified) {
		this.classificationVerified = classificationVerified;
	}

	/**
	 * DB operation functions
	 */
	public static SmsModel getBySmsId(long smsId) {
		return new Select().from(SmsModel.class).where("smsId = ?", smsId).executeSingle();
	}

	public static List<SmsModel> getUnclassified(int numEachSender) {
		List<SmsModel> smsModels = new Select().from(SmsModel.class).where("smsCategoryModel is NULL").orderBy("senderModel").execute();

		List<SmsModel> toReturnList = new ArrayList<>();
		Long currentSender = null;
		int currentSenderCount = 0;
		for (SmsModel smsModel : smsModels) {
			if (smsModel.getSenderModel() == null) {
				Log.i(TAG, "unknown sender found = " + smsModel.getSenderName());
				continue;
			}
			if (currentSender != smsModel.getSenderModel().getId()) {
				currentSenderCount = 0;
				currentSender = smsModel.getSenderModel().getId();
			}
			if (currentSenderCount < numEachSender)
				toReturnList.add(smsModel);
			currentSenderCount++;
		}
		return toReturnList;
	}


	public static List<SmsModel> getClassifiedBySystem() {
		return new Select().from(SmsModel.class).where("categorisedBy = ? and smsCategoryModel is not null", "SYSTEM").execute();
	}

	public static List<SmsModel> getClassifiedByHuman() {
		return new Select().from(SmsModel.class).where("categorisedBy = ? and smsCategoryModel is not null", "HUMAN").execute();
	}


	public static List<SmsModel> getAllUnclassifiedSmsByGroup(SmsCategoryModel categoryModel) {
		List<SenderModel> allSenderInGroup = new Select().from(SenderModel.class).where("groupModel = ?", categoryModel.getGroupModel().getId()).execute();

		List<SmsModel> toReturnList = new ArrayList<>();
		for (SenderModel model : allSenderInGroup) {
			List<SmsModel> smsModels = new Select().from(SmsModel.class).where("senderModel = ?", model.getId()).and("smsCategoryModel is null or categorisedBy != ?", CLASSIFIED_BY.HUMAN).execute();
			if (smsModels != null)
				toReturnList.addAll(smsModels);
		}
		return toReturnList;
	}

	@Override
	public String toString() {
		return "SmsModel{" +
				"smsId=" + smsId +
				", rawData='" + rawData + '\'' +
				", smsCategoryModel=" + smsCategoryModel +
				", senderModel=" + senderModel +
				", receivedTime=" + receivedTime +
				", categorisedBy=" + categorisedBy +
				'}';
	}
}
