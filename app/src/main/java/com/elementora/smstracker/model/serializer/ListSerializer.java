package com.elementora.smstracker.model.serializer;

import com.activeandroid.serializer.TypeSerializer;
import com.google.gson.Gson;

import java.util.List;

/**
 * Created by abhijit on 29/02/16.
 */
public class ListSerializer extends TypeSerializer{
	private static Gson gson = new Gson();

	@Override
	public Class<?> getDeserializedType() {
		return List.class;
	}

	@Override
	public Class<?> getSerializedType() {
		return String.class;
	}

	@Override
	public Object serialize(Object data) {
		if (data == null)
			return null;
		return gson.toJson(data, List.class);
	}

	@Override
	public Object deserialize(Object data) {
		if (data == null)
			return null;
		return gson.fromJson((String) data, List.class);
	}
}
