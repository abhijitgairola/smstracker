package com.elementora.smstracker.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.Date;

/**
 * Created by abhijit on 08/03/16.
 */
@Table(name = "SmsModelUnknown")
public class SmsModelUnknown extends Model {

	@Column(name = "smsId", unique = true, index = true, onUniqueConflict = Column.ConflictAction.FAIL)
	long smsId;

	@Column(name = "rawData")
	String rawData;

	@Column(name = "possibleSender", onDelete = Column.ForeignKeyAction.SET_NULL, onUpdate = Column.ForeignKeyAction.CASCADE, index = true)
	SenderModel possibleSender;

	@Column(name = "receivedTime")
	Date receivedTime;

	public SmsModelUnknown() {
		super();
	}

	public long getSmsId() {
		return smsId;
	}

	public void setSmsId(long smsId) {
		this.smsId = smsId;
	}

	public String getRawData() {
		return rawData;
	}

	public void setRawData(String rawData) {
		this.rawData = rawData;
	}

	public SenderModel getPossibleSender() {
		return possibleSender;
	}

	public void setPossibleSender(SenderModel possibleSender) {
		this.possibleSender = possibleSender;
	}

	public Date getReceivedTime() {
		return receivedTime;
	}

	public void setReceivedTime(Date receivedTime) {
		this.receivedTime = receivedTime;
	}


	public static SmsModelUnknown getBySmsId(long smsId) {
		return new Select().from(SmsModelUnknown.class).where("smsId = ?", smsId).executeSingle();
	}
}
