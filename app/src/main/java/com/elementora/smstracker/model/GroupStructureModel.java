package com.elementora.smstracker.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by abhijit on 08/03/16.
 */
@Table(name = "GroupStructureModel")
public class GroupStructureModel extends Model{

	@Column(name = "groupModel", onDelete = Column.ForeignKeyAction.SET_NULL, onUpdate = Column.ForeignKeyAction.CASCADE, index = true)
	GroupModel groupModel;

	@Column(name = "keywordOccuranceMap")
	Map<String, Integer> keywordOccuranceMap;

	public GroupStructureModel() {
		super();
	}

	public void setKeywordOccuranceMap(Map<String, Integer> keywordOccuranceMap) {
		this.keywordOccuranceMap = keywordOccuranceMap;
	}

	public GroupModel getGroupModel() {
		return groupModel;
	}

	public void setGroupModel(GroupModel groupModel) {
		this.groupModel = groupModel;
	}

	public Map<String, Integer> getKeywordOccuranceMap() {
		return keywordOccuranceMap;
	}
}
