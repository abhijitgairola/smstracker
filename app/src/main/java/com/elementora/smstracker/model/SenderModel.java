package com.elementora.smstracker.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by abhijit on 29/02/16.
 */
@Table(name = "SenderModel")
public class SenderModel extends Model{

	@Column(name = "groupModel", onDelete = Column.ForeignKeyAction.SET_NULL, onUpdate = Column.ForeignKeyAction.CASCADE, index = true, uniqueGroups = {"groupSenderIdx"}, onUniqueConflict = Column.ConflictAction.FAIL)
	GroupModel groupModel;

	@Column(name = "name", uniqueGroups = {"groupSenderIdx"}, onUniqueConflict = Column.ConflictAction.FAIL)
	String name;

	@Column(name = "identifier", unique = true, index = true, onUniqueConflict = Column.ConflictAction.FAIL)
	String identifier;

	public SenderModel() {
		super();
	}

	public GroupModel getGroupModel() {
		return groupModel;
	}

	public void setGroupModel(GroupModel groupModel) {
		this.groupModel = groupModel;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	/**
	DB Operation functions
	 */
	public static SenderModel getByIdentifier(String identifier) {
		return new Select().from(SenderModel.class).where("identifier like ?" , "%"+identifier+"%").executeSingle();
	}

	public static SenderModel getByNameAndGroup(String name, String group) {
		GroupModel groupModel = GroupModel.getByGroupName(group);
		if (groupModel == null)
			return null;
	return new Select().from(SenderModel.class).where("name = ? and groupModel = ?", name, groupModel.getId()).executeSingle();
	}

	public static String[] getAllDistinctSender(String groupName) {

		GroupModel groupModel = GroupModel.getByGroupName(groupName);
		if (groupModel == null)
			return new String[]{};

		List<SenderModel> senderModelList =  new Select().distinct().from(SenderModel.class).where("groupModel = ?", groupModel.getId()).execute();
		String[] toReturn = new String[senderModelList.size()];
		int i = 0;
		for (SenderModel senderModel : senderModelList) {
			toReturn[i++] = senderModel.getName();
		}
		return toReturn;
	}
}
