package com.elementora.smstracker.application;

/**
 * Created by abhijit on 01/03/16.
 */
public class Constants {

	public static final String INITIAL_DB_JOB = "INITIAL_DB_JOB";
	public static final String INBOX_SCAN_JOB = "INBOX_SCAN_JOB";
}
