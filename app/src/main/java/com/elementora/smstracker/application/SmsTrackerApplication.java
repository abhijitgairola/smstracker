package com.elementora.smstracker.application;

import android.app.Application;
import android.content.SharedPreferences;

import com.activeandroid.ActiveAndroid;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by abhijit on 01/03/16.
 */
public class SmsTrackerApplication extends Application {
	private static SmsTrackerApplication instance;

	private SharedPreferences sharedPreferences;
	private Map<String, Boolean> stopwords = new HashMap<>();
	private ArrayList<String> multiStopWords = new ArrayList<>();

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		ActiveAndroid.initialize(this);
		createStopwords();
	}

	public static SmsTrackerApplication getInstance() {
		return instance;
	}

	public SharedPreferences getSharedPreferences() {
		if (sharedPreferences == null){
			sharedPreferences = this.getSharedPreferences("smsTrackerPref", MODE_PRIVATE);
		}
		return sharedPreferences;
	}

	public Map<String, Boolean> getStopwords() {
		if (stopwords == null)
			createStopwords();
		return stopwords;
	}

	public ArrayList<String> getMultiStopWords() {
		return multiStopWords;
	}

	private void createStopwords(){
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(this.getAssets().open("stopwords.txt")));
			String line = br.readLine();

			while (line != null) {
				this.stopwords.put(line, true);
				line = br.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}

		try {
			br = new BufferedReader(new InputStreamReader(this.getAssets().open("multiStopwords.txt")));
			String line = br.readLine();

			while (line != null) {
				this.multiStopWords.add(line);
				line = br.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
}
