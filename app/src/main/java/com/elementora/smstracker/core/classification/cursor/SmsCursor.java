package com.elementora.smstracker.core.classification.cursor;

import android.database.Cursor;

/**
 * Created by abhijit on 29/02/16.
 */
public interface SmsCursor {
	Cursor getCursor();
	String[] getQueryColumns();
}
