package com.elementora.smstracker.core.classification.rules;

/**
 * Created by abhijit on 04/03/16.
 */
public class RuleEngineFactory {
	static RuleEngineFactory instance;

	public static RuleEngineFactory getInstance() {
		if (instance == null){
			synchronized (RuleEngineFactory.class){
				if (instance == null)
					instance = new RuleEngineFactory();
			}
		}
		return instance;
	}

	public RuleEngine getRuleEngine(){
		return new KeywordRuleEngine();
	}
}
