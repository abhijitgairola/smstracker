package com.elementora.smstracker.core.classification.job;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.elementora.smstracker.application.Constants;
import com.elementora.smstracker.application.SmsTrackerApplication;
import com.elementora.smstracker.core.classification.cursor.SmsCursor;
import com.elementora.smstracker.core.classification.logic.ClassificationLogic;
import com.elementora.smstracker.core.classification.logic.LogicFactory;
import com.elementora.smstracker.core.classification.rules.RuleEngine;
import com.elementora.smstracker.core.classification.rules.RuleEngineFactory;
import com.elementora.smstracker.model.GroupModel;
import com.elementora.smstracker.model.SenderModel;
import com.elementora.smstracker.model.SmsModel;
import com.elementora.smstracker.model.SmsCategoryModel;
import com.elementora.smstracker.model.SmsModelUnknown;

import java.util.Date;

/**
 * Created by abhijit on 29/02/16.
 */
public class InboxClassifierJob extends AsyncTask<SmsCursor, Integer, Boolean>{
	private static final String TAG = InboxClassifierJob.class.getSimpleName();

	private Context context;
	ProgressDialog progressDialog;
	JobCallback jobCallback;

	public InboxClassifierJob(Context context, JobCallback jobCallback) {
		this.context = context;
		this.jobCallback = jobCallback;
	}

	public static boolean isInboxScanDone() {
		SharedPreferences sharedPreferences = SmsTrackerApplication.getInstance().getSharedPreferences();
		return sharedPreferences.getBoolean(Constants.INBOX_SCAN_JOB, false);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		progressDialog = ProgressDialog.show(context, "Please wait!", "Reading inbox.");
	}

	@Override
	protected Boolean doInBackground(SmsCursor... params) {
		SmsCursor smsCursor = params[0];
		if (smsCursor != null && smsCursor.getCursor() != null) {
			Cursor cursor = smsCursor.getCursor();
			String[] queryColumns = smsCursor.getQueryColumns();
			cursor.moveToFirst();
			try {
				ActiveAndroid.beginTransaction();
				while (cursor.moveToNext()) {
					long id = cursor.getLong(cursor.getColumnIndex(queryColumns[0]));

					SmsModel smsModel = SmsModel.getBySmsId(id);
					SmsModelUnknown smsModelUnknown = SmsModelUnknown.getBySmsId(id);
					if ((smsModel != null && smsModel.getSmsCategoryModel() != null) || smsModelUnknown != null){
						//We have already read this sms into our database
						continue;
					}

					String smsFrom = cursor.getString(cursor.getColumnIndex(queryColumns[1]));

					String body = cursor.getString(cursor.getColumnIndex(queryColumns[2]));
					long timeS = cursor.getLong(cursor.getColumnIndex(queryColumns[3]));

					ClassificationLogic classificationLogic = LogicFactory.getInstance().getLogic();
					SenderModel senderModel = classificationLogic.identifySender(id, smsFrom);
					if (senderModel == null){
						if (smsModelUnknown == null)
							smsModelUnknown = new SmsModelUnknown();
						smsModelUnknown.setSmsId(id);
						smsModelUnknown.setReceivedTime(new Date(timeS));
						smsModelUnknown.setRawData(body);
						smsModelUnknown.save();

					} else {
						Log.i(TAG, "Reading sms from = " + smsFrom);

						if (smsModel == null)
							smsModel = new SmsModel();
						smsModel.setSmsId(id);
						smsModel.setReceivedTime(new Date(timeS));
						smsModel.setRawData(body);
						smsModel.setSenderModel(senderModel);
						smsModel.setSenderName((smsFrom.split("-").length>1)?smsFrom.split("-")[1]:smsFrom);

						SmsCategoryModel smsCategoryModel = classificationLogic.classify(smsModel);
						smsModel.setSmsCategoryModel(smsCategoryModel);

						smsModel.save();
					}

					/*
					Creating group structure
					 */
					RuleEngine ruleEngine = RuleEngineFactory.getInstance().getRuleEngine();
					ruleEngine.createStructure(new Select().from(GroupModel.class).<GroupModel>execute());

					SharedPreferences sharedPreferences = SmsTrackerApplication.getInstance().getSharedPreferences();
					SharedPreferences.Editor editor = sharedPreferences.edit();
					editor.putBoolean(Constants.INBOX_SCAN_JOB, true);
					editor.commit();

				}
				ActiveAndroid.setTransactionSuccessful();
			} finally {
				cursor.close();
				ActiveAndroid.endTransaction();
			}
		}
		return null;
	}

	@Override
	protected void onPostExecute(Boolean aBoolean) {
		super.onPostExecute(aBoolean);
		progressDialog.dismiss();
		if (jobCallback != null)
			jobCallback.classificationJobDone();
	}

	public interface JobCallback {
		void classificationJobDone();
	}
}
