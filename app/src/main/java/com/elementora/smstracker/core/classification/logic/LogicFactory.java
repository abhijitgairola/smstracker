package com.elementora.smstracker.core.classification.logic;

/**
 * Created by abhijit on 29/02/16.
 */
public class LogicFactory {
	private static LogicFactory instance;

	public static LogicFactory getInstance() {
		if (instance == null){
			synchronized (LogicFactory.class) {
				if (instance == null)
					instance = new LogicFactory();
			}
		}
		return instance;
	}

	public ClassificationLogic getLogic() {
		return new LogicPriority();
	}
}
