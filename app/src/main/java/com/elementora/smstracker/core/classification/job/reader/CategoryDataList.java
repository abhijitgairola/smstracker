package com.elementora.smstracker.core.classification.job.reader;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by abhijit on 01/03/16.
 */
public class CategoryDataList {
	Map<String, ArrayList<CategoryData>> categoryData;

	public Map<String, ArrayList<CategoryData>> getCategoryData() {
		return categoryData;
	}

	public static class CategoryData{
		String categoryName;

		public String getCategoryName() {
			return categoryName;
		}
	}
}
