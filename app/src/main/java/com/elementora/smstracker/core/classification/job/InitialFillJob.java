package com.elementora.smstracker.core.classification.job;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.elementora.smstracker.application.Constants;
import com.elementora.smstracker.application.SmsTrackerApplication;
import com.elementora.smstracker.core.classification.job.reader.CategoryDataList;
import com.elementora.smstracker.core.classification.job.reader.GroupDataList;
import com.elementora.smstracker.core.classification.job.reader.SenderDataList;
import com.elementora.smstracker.model.GroupModel;
import com.elementora.smstracker.model.SenderModel;
import com.elementora.smstracker.model.SmsCategoryModel;
import com.google.gson.Gson;

import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by abhijit on 29/02/16.
 */
public class InitialFillJob extends AsyncTask<Void, Void, Void>{
	private static final String TAG = InitialFillJob.class.getSimpleName();
	private Context context;
	private JobCallback jobCallback;
	ProgressDialog progressDialog;

	public InitialFillJob(Context context, JobCallback jobCallback) {
		this.context = context;
		this.jobCallback = jobCallback;
	}

	public static boolean isInitialRunDone() {
		SharedPreferences sharedPreferences = SmsTrackerApplication.getInstance().getSharedPreferences();
		return sharedPreferences.getBoolean(Constants.INITIAL_DB_JOB, false);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		progressDialog = ProgressDialog.show(context, "Please wait!", "Setting database!");
	}

	@Override
	protected Void doInBackground(Void... params) {

		Gson gson = new Gson();
		try {
			ActiveAndroid.beginTransaction();

			GroupDataList groupDataList = gson.fromJson(new InputStreamReader(context.getAssets().open("group_data.json")), GroupDataList.class);

			/*
			Saving group data
			 */
			for (Map<String, String> groupData : groupDataList.getGroup()){
				GroupModel groupModel = GroupModel.getByGroupName(groupData.get("groupName"));
				if (groupModel != null){
					Log.i(TAG, "groupModel already exists, groupName = " + groupModel.getGroupName());
					continue;
				}
				groupModel = new GroupModel();
				groupModel.setGroupName(groupData.get("groupName"));
				groupModel.setDescription(groupData.get("description"));
				groupModel.setDisplayName(groupData.get("displayName"));
				groupModel.save();
			}

			/*
			Saving sender data
			 */
			SenderDataList senderDataList = gson.fromJson(new InputStreamReader(context.getAssets().open("sender_data.json")), SenderDataList.class);

			for (Map.Entry<String, ArrayList<SenderDataList.SenderData>> entry:  senderDataList.getSenderData().entrySet()) {
				String groupName = entry.getKey();
				GroupModel groupModel = GroupModel.getByGroupName(groupName);
				if (groupName == null) {
					Log.i(TAG, "groupModel does not exists for the sender, groupName = " + groupName);
					continue;
				}

				ArrayList<SenderDataList.SenderData> groupSenderData = entry.getValue();
				for (SenderDataList.SenderData senderData : groupSenderData) {
					SenderModel senderModel = SenderModel.getByIdentifier(senderData.getIdentifier());
					if (senderModel != null) {
						Log.i(TAG, "senderModel already exists, identifier = " + senderModel.getIdentifier());
						continue;
					}
					senderModel = new SenderModel();
					senderModel.setGroupModel(groupModel);
					senderModel.setIdentifier(senderData.getIdentifier());
					senderModel.setName(senderData.getName());
					senderModel.save();
				}
			}

			/*
			Saving category data
			 */
			CategoryDataList categoryDataList = gson.fromJson(new InputStreamReader(context.getAssets().open("sms_category_data.json")), CategoryDataList.class);

			for (Map.Entry<String, ArrayList<CategoryDataList.CategoryData>> entry : categoryDataList.getCategoryData().entrySet()) {
				String groupName = entry.getKey();
				GroupModel groupModel = GroupModel.getByGroupName(groupName);
				if (groupModel == null) {
					Log.i(TAG, "groupModel does not exists, groupModel = " + groupName);
					continue;
				}

				ArrayList<CategoryDataList.CategoryData> senderCategoryData = entry.getValue();
				for (CategoryDataList.CategoryData categoryData : senderCategoryData) {
					SmsCategoryModel smsCategoryModel = new SmsCategoryModel();
					smsCategoryModel.setGroupModel(groupModel);
					smsCategoryModel.setCategoryName(categoryData.getCategoryName());
					smsCategoryModel.save();
				}
			}

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			ActiveAndroid.setTransactionSuccessful();
			SharedPreferences sharedPreferences = SmsTrackerApplication.getInstance().getSharedPreferences();
			SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putBoolean(Constants.INITIAL_DB_JOB, true);
			editor.commit();

		} catch (IOException e) {
			Log.e(TAG, ExceptionUtils.getStackTrace(e));
		} finally {
			ActiveAndroid.endTransaction();
		}

		return null;
	}

	@Override
	protected void onPostExecute(Void aVoid) {
		super.onPostExecute(aVoid);
		progressDialog.dismiss();
		if (jobCallback != null)
			jobCallback.initialDataFillJobDone();
	}

	public interface JobCallback{
		void initialDataFillJobDone();
	}
}
