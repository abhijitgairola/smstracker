package com.elementora.smstracker.core.classification.event;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.activeandroid.query.Select;
import com.elementora.smstracker.core.classification.rules.RuleEngine;
import com.elementora.smstracker.core.classification.rules.RuleEngineFactory;
import com.elementora.smstracker.model.SmsCategoryModel;
import com.elementora.smstracker.model.SmsModel;

import java.util.List;

/**
 * Created by abhijit on 04/03/16.
 */
public class HumanClassifiedEvent{
	private static final String TAG = HumanClassifiedEvent.class.getSimpleName();
	HumanClassifiedEventCallback callback;

	public HumanClassifiedEvent(HumanClassifiedEventCallback callback) {
		this.callback = callback;
	}

	public void onReceive(Context context, Intent intent) {
		Log.i(TAG, "onReceive");
		Bundle bundle = intent.getExtras();
		long smsId = bundle.getLong("smsId");
		SmsModel smsModel = SmsModel.getBySmsId(smsId);
		if (smsModel == null || smsModel.getSmsCategoryModel() == null) {
			return;
		}
		new EngineTask(callback, smsModel).execute();
	}

	private class EngineTask extends AsyncTask<Void, Void, String> {
		HumanClassifiedEventCallback callback;
		SmsModel smsModel;

		public EngineTask(HumanClassifiedEventCallback callback, SmsModel smsModel) {
			this.callback = callback;
			this.smsModel = smsModel;
		}

		@Override
		protected String doInBackground(Void... params) {
			SmsCategoryModel smsCategoryModel = smsModel.getSmsCategoryModel();
			List<SmsModel> smsModelsHumanClassified = new Select().from(SmsModel.class).where("smsCategoryModel = ?", smsCategoryModel.getId()).and("categorisedBy = ?", "HUMAN").execute();
			if (smsModelsHumanClassified.size() < 5) {
				return "Data not sufficient enough to create rule.";
			}

			RuleEngine ruleEngine = RuleEngineFactory.getInstance().getRuleEngine();
			try{
				ruleEngine.createRule(smsModelsHumanClassified, smsCategoryModel);
			} catch (Exception e) {
				return "System is unable to find similarities for rule creation.";
			}

			return "Rules created successfully";
		}

		@Override
		protected void onPostExecute(String message) {
			if (callback != null)
				callback.actionCompleted(message);
		}
	}
}
