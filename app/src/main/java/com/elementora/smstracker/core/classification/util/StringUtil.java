package com.elementora.smstracker.core.classification.util;

import com.elementora.smstracker.application.SmsTrackerApplication;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by abhijit on 04/03/16.
 */
public class StringUtil {

	public static List<String> getCommonWords(List<String> lineList) {
		Map<String, Integer> countMap = getCountMap(lineList);

		int totalLines = lineList.size();
		List<String> commonWords = new ArrayList<>();
		for (Map.Entry<String, Integer> entry : countMap.entrySet()) {
			if (entry.getValue() >= totalLines)
				commonWords.add(entry.getKey());
		}
		return commonWords;
	}

	public static Map<String, Integer> getKeywordFrequency(List<String> lines) {
		Map<String, Integer> countMap = getCountMap(lines);

		Map<String, Integer> toReturn = new HashMap<>();

		int totalLines = lines.size();
		for (Map.Entry<String, Integer> entry : countMap.entrySet()) {
			int count = entry.getValue();
			int percentage = (count * 100)/totalLines;
			toReturn.put(entry.getKey(), percentage);
		}
		return toReturn;
	}


	private static Map<String, Integer> getCountMap(List<String> lines) {
		Map<String, Integer> countMap = new HashMap<>();
		for (String line : lines) {
			String[] words = line.split("\\s+");
			Map<String, Boolean> duplicateWords = new HashMap<>();
			for (String word : words) {
				word = StringUtils.replace(word, ",", "");
				word = StringUtils.replace(word, ".", "");
				word = StringUtils.lowerCase(word);
				if (duplicateWords.containsKey(word))
					continue;
				if (countMap.containsKey(word))
					countMap.put(word, countMap.get(word) + 1);
				else
					countMap.put(word, 1);
				duplicateWords.put(word, true);
			}
		}
		return countMap;
	}

	public static String removeAllStopwords(String line) {
		line = StringUtils.replace(line, ",", " ");
		line = StringUtils.replace(line, ".", " ");
		line = StringUtils.lowerCase(line);

//		line = line.replaceAll("\\d{0,}", "");
//		line = line.replaceAll("\\+[x]", "");

		line = line.replaceAll("[^a-z\\s]+", "");
		line = line.replaceAll("\\b[x]+\\b", "");

		for (String multiStopWords : SmsTrackerApplication.getInstance().getMultiStopWords()) {
			line = line.replaceAll("\\\\"+multiStopWords+"\\b", multiStopWords.replaceAll("\\s+", ""));
		}

		String[] words = line.split("\\s+");

		Map<String, Boolean> stopWords = SmsTrackerApplication.getInstance().getStopwords();

		StringBuilder stringBuilder = new StringBuilder();
		for (String word : words) {
			if (stopWords.containsKey(word)){
				continue;
			}
			stringBuilder.append(word + " ");
		}
		return stringBuilder.toString().trim();
	}

	public static void main(String[] args) {
		List<String> test = new ArrayList<>();
		test.add("AB CD EF, GH IJ KL AB");
		test.add("AB EF GH IJ PQ MR");
		test.add("EF GH XY ZZ ab");

		System.out.println(getCommonWords(test));

		String sms = "Rs 280.00 xxxxxxxx was spent on your Credit Card 4386XXXXXXX0797 on 03-MAR-16 at ULTRACASH TECHNOLOGI";

		sms = sms.replaceAll("\\b[x]+\\b", "");
		System.out.println(sms);
	}
}
