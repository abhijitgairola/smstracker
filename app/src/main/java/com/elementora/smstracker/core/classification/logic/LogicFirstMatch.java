package com.elementora.smstracker.core.classification.logic;

import com.activeandroid.query.Select;
import com.elementora.smstracker.core.classification.util.StringUtil;
import com.elementora.smstracker.model.ClassificationRuleModel;
import com.elementora.smstracker.model.SmsCategoryModel;
import com.elementora.smstracker.model.SmsModel;
import com.elementora.smstracker.model.data.ClassificationRule;

import java.util.List;

/**
 * Created by abhijit on 29/02/16.
 */
public class LogicFirstMatch extends ClassificationLogic {
	@Override
	public SmsCategoryModel classify(SmsModel smsModel) {
		List<SmsCategoryModel> possibleCategories = this.getPossibleCategories(smsModel);
		SmsCategoryModel classifiedCategory = null;
		for (SmsCategoryModel smsCategoryModel : possibleCategories) {
			ClassificationRuleModel ruleModel = new Select().from(ClassificationRuleModel.class).where("smsCategoryModel = ?", smsCategoryModel.getId()).executeSingle();
			ClassificationRule rule = ruleModel.getClassificationRule();
			if (ruleModel == null)
				continue;
			String cleanedSms = this.getCleanedData(smsModel.getRawData());
			boolean allKeywordsMatched = true;
			for (String wantedKeyword : rule.getWantedKeywords()) {
				if (cleanedSms.indexOf(wantedKeyword) == -1) {
					allKeywordsMatched = false;
					break;
				}
			}
			if (allKeywordsMatched == true) {
				classifiedCategory = smsCategoryModel;
				break;
			}
		}

		this.postClassification(smsModel, classifiedCategory);
		return classifiedCategory;
	}
}
