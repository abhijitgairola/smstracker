package com.elementora.smstracker.core.classification.rules;

import com.activeandroid.query.Select;
import com.elementora.smstracker.core.classification.logic.ClassificationLogic;
import com.elementora.smstracker.core.classification.logic.LogicFactory;
import com.elementora.smstracker.core.classification.util.StringUtil;
import com.elementora.smstracker.model.ClassificationRuleModel;
import com.elementora.smstracker.model.GroupModel;
import com.elementora.smstracker.model.GroupStructureModel;
import com.elementora.smstracker.model.SenderModel;
import com.elementora.smstracker.model.SmsCategoryModel;
import com.elementora.smstracker.model.SmsModel;
import com.elementora.smstracker.model.data.ClassificationRule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by abhijit on 04/03/16.
 */
public class KeywordRuleEngine implements RuleEngine {
	@Override
	public void createRule(List<SmsModel> smsModels, SmsCategoryModel smsCategoryModel) {
		List<String> rawSmsList = new ArrayList<>();
		for (SmsModel smsModel : smsModels) {
			rawSmsList.add(StringUtil.removeAllStopwords(smsModel.getRawData()));
		}

		List<String> commonWords = StringUtil.getCommonWords(rawSmsList);

		//TODO: need to handle the case where no common keywords
		if (commonWords.size() == 0)
			throw new RuntimeException("No similarity found. Not able to create classification rule.");

		ClassificationRuleModel classificationRuleModel = new Select().from(ClassificationRuleModel.class).where("smsCategoryModel = ?", smsCategoryModel.getId()).executeSingle();

		if (classificationRuleModel == null)
			classificationRuleModel = new ClassificationRuleModel();

		classificationRuleModel.setSmsCategoryModel(smsCategoryModel);

		ClassificationRule classificationRule = new ClassificationRule(commonWords, null, null, null);
		classificationRuleModel.setClassificationRule(classificationRule);
		classificationRuleModel.save();

		/*
		Filling unwanted list
		 */
		this.fillUnwantedKeywords(classificationRuleModel);

		this.classifyAfterRuleCreation(smsCategoryModel, classificationRuleModel);
	}

	private void classifyAfterRuleCreation(SmsCategoryModel smsCategoryModel, ClassificationRuleModel classificationRuleModel) {
		List<SmsModel> unClassifiedSms = SmsModel.getAllUnclassifiedSmsByGroup(smsCategoryModel);

		if (unClassifiedSms != null){
			ClassificationLogic classificationLogic = LogicFactory.getInstance().getLogic();
			for (SmsModel smsModel : unClassifiedSms) {
				classificationLogic.classify(smsModel);
			}
		}
	}


	private void fillUnwantedKeywords(ClassificationRuleModel newRule) {
		List<ClassificationRuleModel> oldRules = ClassificationRuleModel.getAllRulesByGroup(newRule.getSmsCategoryModel());
		List<String> unwantedKeywordsForNewRule = new ArrayList<>();
		for (ClassificationRuleModel ruleModel : oldRules) {
			if (ruleModel.getId() == newRule.getId()) {
				continue;
			}
			List<String> unwantedKeywords = ruleModel.getClassificationRule().getUnwantedKeywords();
			if (unwantedKeywords == null)
				unwantedKeywords = new ArrayList<>();
			unwantedKeywords.addAll(newRule.getClassificationRule().getWantedKeywords());
			ruleModel.getClassificationRule().setUnwantedKeywords(unwantedKeywords);
			ruleModel.save();

			unwantedKeywordsForNewRule.addAll(ruleModel.getClassificationRule().getWantedKeywords());
		}

		List<String> unwantedKeywords = newRule.getClassificationRule().getUnwantedKeywords();
		if (unwantedKeywords == null)
			unwantedKeywords = new ArrayList<>();
		unwantedKeywords.addAll(unwantedKeywordsForNewRule);
		newRule.getClassificationRule().setUnwantedKeywords(unwantedKeywords);
		newRule.save();
	}


	@Override
	public void createStructure(List<GroupModel> groupModels) {
		for (GroupModel groupModel : groupModels) {
			this.createStructure(groupModel);
		}
	}

	@Override
	public void createStructure(GroupModel groupModel) {
		List<SenderModel> senderModels = new Select().from(SenderModel.class).where("groupModel = ?", groupModel.getId()).execute();

		List<String> smsInGroup = new ArrayList<>();
		for (SenderModel senderModel : senderModels) {
			List<SmsModel> smsModels = new Select().from(SmsModel.class).where("senderModel = " + senderModel.getId()).execute();
			if (smsModels != null){
				for (SmsModel smsModel : smsModels) {
					smsInGroup.add(StringUtil.removeAllStopwords(smsModel.getRawData()));
				}
			}
		}

		Map<String, Integer> keywordFrequency = StringUtil.getKeywordFrequency(smsInGroup);

		GroupStructureModel groupStructureModel = new Select().from(GroupStructureModel.class).where("groupModel = ?", groupModel.getId()).executeSingle();
		if (groupStructureModel == null)
			groupStructureModel = new GroupStructureModel();

		groupStructureModel.setGroupModel(groupModel);
		groupStructureModel.setKeywordOccuranceMap(keywordFrequency);
		groupStructureModel.save();
	}
}
