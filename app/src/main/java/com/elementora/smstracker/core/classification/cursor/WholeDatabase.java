package com.elementora.smstracker.core.classification.cursor;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

/**
 * Created by abhijit on 29/02/16.
 */
public class WholeDatabase implements SmsCursor {

	private Context context;

	public WholeDatabase(Context context) {
		this.context = context;
	}

	private static String[] queryColumns = new String[]{"_id", "address", "body", "date"};

	@Override
	public Cursor getCursor() {
		String SORT_ORDER = "date DESC";

		Uri uri = Uri.parse("content://sms/inbox");
		return context.getContentResolver().query(uri, queryColumns, null, null, SORT_ORDER);
	}

	@Override
	public String[] getQueryColumns() {
		return queryColumns;
	}
}
