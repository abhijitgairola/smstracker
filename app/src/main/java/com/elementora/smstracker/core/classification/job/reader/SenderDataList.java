package com.elementora.smstracker.core.classification.job.reader;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by abhijit on 01/03/16.
 */
public class SenderDataList {
	Map<String, ArrayList<SenderData>> senderData;

	public Map<String, ArrayList<SenderData>> getSenderData() {
		return senderData;
	}

	public static class SenderData{
		String name, identifier;

		public String getIdentifier() {
			return identifier;
		}

		public String getName() {
			return name;
		}
	}
}
