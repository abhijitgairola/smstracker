package com.elementora.smstracker.core.classification.logic;

import com.activeandroid.query.Select;
import com.elementora.smstracker.core.classification.util.StringUtil;
import com.elementora.smstracker.model.ClassificationRuleModel;
import com.elementora.smstracker.model.SmsCategoryModel;
import com.elementora.smstracker.model.SmsModel;
import com.elementora.smstracker.model.data.ClassificationRule;

import java.util.List;

/**
 * Created by abhijit on 29/02/16.
 */
public class LogicPriority extends ClassificationLogic{
	@Override
	public SmsCategoryModel classify(SmsModel smsModel) {
		List<SmsCategoryModel> possibleCategories = this.getPossibleCategories(smsModel);
		SmsCategoryModel classifiedCategory = null;

		int lastPriority = Integer.MIN_VALUE;
		for (SmsCategoryModel categoryModel : possibleCategories) {
			ClassificationRuleModel ruleModel = new Select().from(ClassificationRuleModel.class).where("smsCategoryModel = ?", categoryModel.getId()).executeSingle();
			if (ruleModel == null || ruleModel.getClassificationRule() == null)
				continue;

			ClassificationRule rule = ruleModel.getClassificationRule();
			String cleanedSms = this.getCleanedData(smsModel.getRawData());

			int currentPriority = 0;
			for (String wantedKeywords : rule.getWantedKeywords()) {
				int index = cleanedSms.indexOf(wantedKeywords);
				if (index == -1)
					continue;
				currentPriority += (200 - index);
			}
			if (lastPriority < currentPriority && currentPriority > 0) {
				lastPriority = currentPriority;
				classifiedCategory = categoryModel;
			}
		}
		this.postClassification(smsModel, classifiedCategory);
		return classifiedCategory;
	}
}
