package com.elementora.smstracker.core.classification.event;

/**
 * Created by abhijit on 05/03/16.
 */
public interface HumanClassifiedEventCallback {
	void actionCompleted(String message);
}
