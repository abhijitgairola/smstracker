package com.elementora.smstracker.core.classification.rules;

import com.elementora.smstracker.model.GroupModel;
import com.elementora.smstracker.model.SmsCategoryModel;
import com.elementora.smstracker.model.SmsModel;

import java.util.List;

/**
 * Created by abhijit on 04/03/16.
 */
public interface RuleEngine {
	void createRule(List<SmsModel> smsModels, SmsCategoryModel smsCategoryModel);
	void createStructure(List<GroupModel> groupModels);
	void createStructure(GroupModel groupModel);
}
