package com.elementora.smstracker.core.classification.event;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.telephony.SmsMessage;

import com.elementora.smstracker.application.Constants;
import com.elementora.smstracker.application.SmsTrackerApplication;

/**
 * Created by abhijit on 06/03/16.
 */
public class SmsReceiver extends WakefulBroadcastReceiver {

	public static final String SMS_BUNDLE = "pdus";

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent == null ? null : intent.getAction();
		if (Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(action)) {

			startWakefulService(context, intent);
			Bundle bundle = intent.getExtras(); // ---get the SMS message passed
			// in---

			SmsMessage[] msgs = null;
			String msg_from = null;
			if (bundle != null) {
				// ---retrieve the SMS message received---
				try {
					Object[] pdus = (Object[]) bundle.get(SMS_BUNDLE);
					msgs = new SmsMessage[pdus.length];

					long time1 = System.currentTimeMillis();
					StringBuilder smsBody = new StringBuilder();
					for (int i = 0; i < msgs.length; i++) {
						msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
						msg_from = msgs[i].getOriginatingAddress();
						String msgBody = msgs[i].getMessageBody();
						smsBody.append(msgBody);
						time1 = msgs[i].getTimestampMillis();
					}
					String sms = smsBody.toString();

					SharedPreferences sharedPreferences = SmsTrackerApplication.getInstance().getSharedPreferences();
					SharedPreferences.Editor editor = sharedPreferences.edit();
					editor.putBoolean(Constants.INBOX_SCAN_JOB, false);
					editor.commit();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			completeWakefulIntent(intent);

		}
	}
}
