package com.elementora.smstracker.core.classification.logic;

import com.activeandroid.query.Select;
import com.elementora.smstracker.core.classification.util.StringUtil;
import com.elementora.smstracker.model.GroupModel;
import com.elementora.smstracker.model.SenderModel;
import com.elementora.smstracker.model.SmsCategoryModel;
import com.elementora.smstracker.model.SmsModel;
import com.elementora.smstracker.model.SmsModelUnknown;

import java.util.List;

/**
 * Created by abhijit on 29/02/16.
 */
public abstract class ClassificationLogic {
	public abstract SmsCategoryModel classify(SmsModel smsModel);

	public abstract GroupModel classifyUnknownIntoGroups(SmsModelUnknown smsModelUnknown);

	public SenderModel identifySender(long smsId, String sender){
		sender = (sender.split("-").length > 1)?sender.split("-")[1]:sender;
		return SenderModel.getByIdentifier(sender);
	}

	protected List<SmsCategoryModel> getPossibleCategories(SmsModel smsModel) {
		return new Select().from(SmsCategoryModel.class).where("groupModel = ?", smsModel.getSenderModel().getGroupModel().getId()).execute();
	}

	protected String getCleanedData(String sms) {
		if (sms == null)
			return null;
		return StringUtil.removeAllStopwords(sms);
	}

	protected void postClassification(SmsModel smsModel, SmsCategoryModel smsCategoryModel) {
		if (smsCategoryModel == null || smsModel == null)
			return;
		smsModel.setSmsCategoryModel(smsCategoryModel);
		smsModel.setCategorisedBy(SmsModel.CLASSIFIED_BY.SYSTEM);
		smsModel.save();
	}
}
